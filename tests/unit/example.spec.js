import { shallowMount } from '@vue/test-utils';
import HelloWorld from '@/components/HelloWorld.vue';

describe('HelloWorld.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message';
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });

  it('should update name', () => {
    const msg = 'Hello';

    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg },
    });

    wrapper.find('button').trigger('click');

    expect(wrapper.text()).toMatch('Hello DevOpsDays Istanbul');
  });
});
